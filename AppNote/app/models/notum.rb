class Notum < ApplicationRecord

	attr_reader :categories
	attr_reader :userShared
	attr_reader :aux_user
  	#belongs_to :user
	has_many :has_categories,  dependent:   :destroy
 	#has_many :categories, through: :has_categories
	after_create :save_categories
	after_create :save_userShared
	after_update :update_categories
  	after_update :update_userShared
	has_many :note_shareds
	has_many :NoteShared
	has_many :user, through: :note_shareds

	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/img/no_Image.png", validate_media_type: false
   	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

	def categories=(value)
		@categories=value
	end
	def userShared=(value)
	    @userShared=value
	end

	def aux_user=(value)
		@aux_user = value
	end

	private
	def save_categories
		if @categories != nil
			@categories.each do |category_id|
				HasCategory.create(category_id: category_id,notum_id: self.id)
			end
		end
	end

	def update_categories
		if @categories == nil
			aux = HasCategory.find_by notum_id: self.id
			if aux != nil
			#HasCategory.update(aux.id, :category_id  => nil,:notum_id  => self.id)
				aux.destroy
			end
		end
		if @categories != nil
			@categories.each do |category_id|
				if  !HasCategory.exists?(:notum_id => self.id)
					HasCategory.create(category_id: category_id,notum_id: self.id)
				end
			end
		end
	end

	  def save_userShared
	    NoteShared.create(user_id: @aux_user,notum_id: self.id)
	  	if @userShared != nil
	      @userShared.each do |user_id|
	        NoteShared.create(user_id: user_id,notum_id: self.id)
	      end
	    end
	  end

	def update_userShared
	  	if @userShared != nil
	      @userShared.each do |user_id|
	        NoteShared.create(user_id: user_id,notum_id: self.id)
	      end
	    end
	end

end
