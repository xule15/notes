class Category < ApplicationRecord
  belongs_to :user
  has_many :has_categories ,dependent:   :destroy
  has_many :notum, through: :has_categories
end
