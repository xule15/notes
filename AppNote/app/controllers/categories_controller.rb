class CategoriesController < InheritedResources::Base
#before_action :set_categories
before_action :authenticate_user!


def create

  @categories = current_user.Category.new(category_params)


  respond_to do |format|
    if @categories.save
      format.html { redirect_to @categories, notice: 'Notum was successfully created.' }
      format.json { render :show, status: :created, location: @categories }
    else
      format.html { render :new }
      format.json { render json: @categories.errors, status: :unprocessable_entity }
    end
  end
end

 #private

   def category_params
     params.require(:category).permit(:name)
   end
#end


end
