Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  resources :categories
  resources :user

  devise_for :users
  get 'welcome/index'

  resources :nota
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #root 'welcome#index'
  #root  'sessions#new'

  devise_scope :user do
    authenticated :user do
      root 'welcome#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end



   resources :user do
    member do
      get :following, :followers
    end
  end

  resources :relationships,       only: [:create, :destroy]


end
