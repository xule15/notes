class AddUserIdToNota < ActiveRecord::Migration[5.1]
  def change
    add_reference :nota, :user, foreign_key: true
  end
end
