class CreateNoteShareds < ActiveRecord::Migration[5.1]
  def change
    create_table :note_shareds do |t|
      t.references :user, foreign_key: true
      t.references :notum, foreign_key: true

      t.timestamps
    end
  end
end
